#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 1    last modified 2015-06-07 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2015
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set border 1
set nogrid
set key autotitles nobox reverse Left enhanced vert left at graph 0, screen 0.98

set noytics
unset colorbox

set xtics 5 out nomirror rotate by -45

set yrange[0:0.7]
set size ratio 0.3


set style arrow 1 heads fill size screen .02,15 lc 'gray'
set style arrow 2 nohead lc 'gray'

_w = 1280
_h = (_w * 0.39) * 2

# a helper function
max(a, b) = (a > b) ? a : b

symbols_distances_data = 'symbols_distances_defaults.log'

# Some other example
#symbols_distances_data = 'symbols_distances_optimistic.log'
#symbols_distances_data = 'symbols_distances_codex.log'
#symbols_distances_data = 'symbols_distances_savemysugar.log'

set terminal unknown

plot symbols_distances_data using 7:9
call_setup_time_min = GPVAL_DATA_X_MAX
call_setup_time_max = GPVAL_DATA_Y_MAX

plot symbols_distances_data using 0:20
num_symbols = GPVAL_DATA_X_MAX + 1
dot_distance_min = GPVAL_DATA_Y_MIN
EOM_distance_min = GPVAL_DATA_Y_MAX

plot symbols_distances_data using 0:16
symbol_distance_min = GPVAL_DATA_X_MIN
inter_call_distance = GPVAL_DATA_Y_MIN

symbol_distance = 2 * (call_setup_time_max - call_setup_time_min)
call_position = dot_distance_min - inter_call_distance

max_x = EOM_distance_min + symbol_distance

get_val(val) = val eq "inf" ? max_x : val
call_time(sym_dist) = abs(call_setup_time_max - first_call_time) + sym_dist + abs(second_call_time - call_setup_time_min)
symbol_bar_height(val) = (num_symbols - $0) / (2 * num_symbols)

set xrange [0:max_x + 3]
set yrange[0:0.6]

set terminal qt 0 enhanced font "Georgia,14" size _w,_h
#set terminal pngcairo notransparent  nocrop truecolor rounded enhanced font "Georgia,14" fontscale 1.0 size _w,_h
#set output 'symbols_distances.png'

my_title = "Relative symbol distances (receiver)\n" . \
         sprintf("min call setup time %.2f s\n", call_setup_time_min) . \
         sprintf("max call setup time %.2f s\n", call_setup_time_max) . \
         sprintf("fixed inter call distance %.2f s\n", inter_call_distance)

set multiplot layout 2, 1 title my_title font "Georgia,14"

first_call_time = call_setup_time_max
second_call_time = call_setup_time_min

set title "Minimum distance between the first rings of two calls"
plot \
  symbols_distances_data using (0):(0):(call_position):(call_position + call_time($16)):(0):(symbol_bar_height($0)):0 with boxxy \
    fs solid noborder lc palette notitle, \
  symbols_distances_data using ($20):(0.53):(get_val(strcol(22)) - $20):(0) with vectors arrowstyle 1 title 'receiver symbol window', \
  symbols_distances_data using ($20):(0):(0):(0.55) with vectors arrowstyle 2 notitle, \
  symbols_distances_data using ($20 + symbol_distance / 2):(0.55):(strcol(11)) with labels font "Courier,12" notitle, \

first_call_time = call_setup_time_min
second_call_time = call_setup_time_max

set title "Maximum distance between the first rings of two calls"
replot

unset multiplot
