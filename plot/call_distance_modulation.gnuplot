#!/usr/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.0 patchlevel 1    last modified 2015-06-07 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2015
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')

set border 1
set nogrid
set key autotitles nobox reverse Left enhanced vert left at graph 0, screen 0.98

set title "Morse code sent using phone rings and pulse-distance modulation"
set xlabel "time since the first call (seconds)"

set noytics

set xtics 30 out nomirror rotate by -45 font "Georgia,12"

set yrange[0:0.7]
set size ratio 0.3

set style arrow 1 heads size 2,90 front ls 201 lc 'gray'

_w = 1280
_h = _w * 0.39

# a helper function
min(a, b) = (a < b) ? a : b

# The data files
transmit_data="codex_transmit.log"
receive_data="codex_receive.log"
#transmit_data="savemysugar_transmit.log"
#receive_data="savemysugar_receive.log"

#transmit_data="android_codex_transmit.log"
#receive_data="android_codex_receive.log"
#transmit_data="android_savemysugar_transmit.log"
#receive_data="android_savemysugar_receive.log"

#transmit_data="android_SendFragment_savemysugar_transmit.log"
#receive_data="android_SendFragment_savemysugar_receive.log"
#transmit_data="android_ReceiveFragment_savemysugar_transmit.log"
#receive_data="android_ReceiveFragment_savemysugar_receive.log"

set terminal unknown

plot transmit_data using 1:(1)
sender_t0 = GPVAL_DATA_X_MIN

plot receive_data using 1:(1)
receiver_t0 = GPVAL_DATA_X_MIN

t0 = min(sender_t0, receiver_t0)

morse_symbol(symbol) = symbol eq " " ? "sep" : symbol eq "/" ? "SEP" : symbol
end_of_signal(symbol) = (symbol eq " ") || (symbol eq "/") || (symbol eq "EOM")
text_character(symbol, character) = symbol eq "/" ? character . "␣" : character

set terminal qt 0 enhanced font "Georgia,14" size _w,_h
#set terminal pngcairo notransparent  nocrop truecolor rounded enhanced font "Georgia,14" fontscale 1.0 size _w,_h
#set output 'pulse_distance_modulation.png'

plot \
  transmit_data using (0):(0):($1 - t0):($1 - t0 + $8):(0):(0.5) with boxxy \
    fs solid noborder lc "#90caf9" title 'outgoing calls', \
  receive_data using ($1 - t0):(0.5) with impulses lw 2 lc '#ff3d00' title 'incoming rings', \
  receive_data using ($4 > 0 ? $1 - t0 : 1/0):(0.53):(-$4):(0) with vectors arrowstyle 1 notitle, \
  receive_data using ($4 > 0 ? ($1 - t0) - ($4 / 2) : 1/0):(0.56):(morse_symbol(strcol(6))) with labels notitle, \
  receive_data using (end_of_signal(strcol(6)) ? ($1 - t0) : 1/0):(0.61):(text_character(strcol(6), strcol(8))) with labels font "Courier,14" notitle, \
