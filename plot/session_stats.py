#!/usr/bin/env python3
#
# session_stats.py - gather stats about a SaveMySugar session
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import sys
import logging


def session_stats(transmitter_log_filename, receiver_log_filename):
    transmitter_log = open(transmitter_log_filename)
    receiver_log = open(receiver_log_filename)

    transmitter_dial_times = []
    for line in transmitter_log:
        transmitter_dial_times.append(float(line.split(" ")[0]))

    receiver_ring_times = []
    for line in receiver_log:
        receiver_ring_times.append(float(line.split(" ")[0]))

    if len(transmitter_dial_times) != len(receiver_ring_times):
        logging.error("The two files must contain the same number of lines")
        sys.exit(1)

    min_call_setup_time = 10000
    max_call_setup_time = -1
    for dial_time, ring_time in zip(transmitter_dial_times,
                                    receiver_ring_times):
        call_setup_time = ring_time - dial_time
        max_call_setup_time = max(max_call_setup_time, call_setup_time)
        min_call_setup_time = min(min_call_setup_time, call_setup_time)
        logging.info("call setup time: %9.6f", call_setup_time)

    return min_call_setup_time, max_call_setup_time


def main():
    if len(sys.argv) != 3:
        print("usage: %s <transmit_log> <receive_log>" % sys.argv[0])
        sys.exit(1)

    call_setup_min, call_setup_max = session_stats(sys.argv[1], sys.argv[2])

    print("\nSession stats:")
    print("min call setup time: %9.6f" % call_setup_min)
    print("max call setup time: %9.6f" % call_setup_max)


if __name__ == "__main__":
    main()
