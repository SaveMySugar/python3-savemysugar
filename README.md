SaveMySugar
===========

SaveMySugar is an experiment about transmitting and receiving messages for free
using phone call rings to encode Morse code.

The name SaveMySugar is a pun on the price per bit of SMSs (Short Message
Service) which is quite high, and the distress signal Save Our Soul (SOS) used
in Morse communications.

See [http://savemysugar.ao2.it](http://savemysugar.ao2.it) for further details.

Tutorial
--------

The "measure" pass in 1. and 2. assumes that there are two serial modems
attached to the same host connected to two independent phone lines, so that
a single program can control both the transmitter and the receiver device.

A possible setup is to have two cell phones providing serial ports over which
the programs can send AT commands and receive RING notifications.

1. Measure the call setup time using `src/measure_call_setup_time.py` and take
   note of `Min call setup time` and `Max call setup time`.

2. Measure the distance between rings in the same call using
   `src/measure_ring_distance.py` and take note of `Min ring distance` and
   `Max ring distance`.

3. Adjust the parameters in `src/savemysugar/CallDistanceTransceiver.py` using
   the values measured before, possibly approximating the minima by defect and
   the maxima by excess.

4. Transmit and/or receive a message using `src/transmit.py` or `src/receive.py`

The sender and the receiver must use the same parameters.

In a future version it should be possible to pass the parameters as command line
arguments to `transmit.py` and `receive.py`, but for now these values have to be
hardcoded.

Dependencies
------------

The only dependencies are:

* python3
* python3-serial

Authors
-------

From an idea by Corrado Rubera.
Written by Antonio Ospite <ao2@ao2.it>.
