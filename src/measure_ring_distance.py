#!/usr/bin/env python3
#
# measure_ring_distance - measure the time between two rings in the same call
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time

from savemysugar.cumulative_average import cumulative_average
from savemysugar.Modem import Modem


class Rings(object):
    previous_time = -1
    average_distance = 0
    min_distance = 10000
    max_distance = -1
    count = 0


def on_ring():
    Rings.count += 1

    if Rings.previous_time == -1:
        print("First ring")
        Rings.previous_time = time.time()
    else:
        new_time = time.time()
        distance = new_time - Rings.previous_time
        Rings.previous_time = new_time
        Rings.min_distance = min(Rings.min_distance, distance)
        Rings.max_distance = max(Rings.max_distance, distance)
        Rings.average_distance = cumulative_average(Rings.average_distance,
                                                    Rings.count - 1, distance)
        print()
        print("Other ring")
        print("Ring distance: %f" % distance)
        print("Min ring distance: %f" % Rings.min_distance)
        print("Max ring distance %f" % Rings.max_distance)
        print("Average distance: %f" % Rings.average_distance)


def measure_ring_distance(ingoing_port, outgoing_port, destination_number):
    ingoing_modem = Modem(ingoing_port)
    ingoing_modem.register_callback("RING", on_ring)
    outgoing_modem = Modem(outgoing_port)

    outgoing_modem.send_command("ATDT" + destination_number + ";")

    try:
        ingoing_modem.get_response_loop()
    except KeyboardInterrupt:
        outgoing_modem.send_command("ATH")
        outgoing_modem.get_response()


def main():
    import sys
    if len(sys.argv) != 4:
        print("usage: %s" % sys.argv[0],
              "<ingoing serial port> <outgoing serial port>",
              "<destination number>")
        sys.exit(1)

    measure_ring_distance(sys.argv[1], sys.argv[2], sys.argv[3])


if __name__ == "__main__":
    main()
