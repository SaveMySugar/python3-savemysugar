#!/usr/bin/env python3
#
# measure_call_setup_time_from_android - call setup time from Android to PC
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# NOTE: since there is no easy way to hangup an Android call from this program,
# so the suggested way to use it is to set an high value for the "Maximum call
# setup time" (e.g. 17) and then send a message from the Android app while
# measuring the times with this program.

import subprocess
import time

from savemysugar.cumulative_average import cumulative_average
from savemysugar.Modem import Modem


class Ring(object):
    time = -1


def on_ring():
    if Ring.time == -1:
        Ring.time = time.time()
        raise StopIteration("Got the ring we were waiting for")


def measure_call_setup_time_from_android(incoming_port):
    incoming_modem = Modem(incoming_port)
    incoming_modem.register_callback("RING", on_ring)

    min_call_setup_time = 10000
    max_call_setup_time = -1

    avg_call_setup_time = 0

    subprocess.check_call("adb logcat -c".split())
    logcat = subprocess.Popen("adb logcat -s SaveMySugar:*".split(),
                              stdout=subprocess.PIPE)

    i = 0
    while True:
        print()
        print("Call: %s" % (i + 1))

        # When an outgoing call is placed, the SaveMySugar Android App
        # outputs a line like this:
        # D/SaveMySugar( 1604): 1451818183.802000 sendSymbol Dial and wait ...
        for line in logcat.stdout:
            line_str = line.decode('UTF-8')
            if "sendSymbol Dial" in line_str:
                dial_time = time.time()
                break

        # Wait for the receiver to get a ring
        try:
            # flush to discard unprocessed RING notifications this is necessary
            # in this case because we don't hang up the transmitter explicitly,
            # so it may happen that it sends multiple rings for each call.
            incoming_modem.flush()
            incoming_modem.get_response_loop()
        except StopIteration:
            pass

        call_setup_time = Ring.time - dial_time
        min_call_setup_time = min(min_call_setup_time, call_setup_time)
        max_call_setup_time = max(max_call_setup_time, call_setup_time)
        avg_call_setup_time = cumulative_average(avg_call_setup_time,
                                                 i + 1,
                                                 call_setup_time)
        print("Call setup time: %f" % call_setup_time)
        print("Min call setup time: %f" % min_call_setup_time)
        print("Max call setup time: %f" % max_call_setup_time)
        print("Average call setup time: %f" % avg_call_setup_time)
        print()

        Ring.time = -1
        i += 1


def main():
    import sys
    if len(sys.argv) != 2:
        print("usage: %s" % sys.argv[0], "<incoming serial port>")
        sys.exit(1)

    measure_call_setup_time_from_android(sys.argv[1])


if __name__ == "__main__":
    main()
