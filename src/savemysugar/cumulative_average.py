#!/usr/bin/env python3
#
# cumulative_average - compute the cumulative average
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


def cumulative_average(old_average, n_samples, new_sample):
    return ((old_average * (n_samples - 1)) + new_sample) / n_samples


def test():
    values = [1, 2, 3, 4, 5, 6]
    average = 0
    for i, value in enumerate(values):
        average = cumulative_average(average, i + 1, value)
        print(average)

if __name__ == "__main__":
    test()
