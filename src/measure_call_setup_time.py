#!/usr/bin/env python3
#
# measure_call_setup_time - measure time between dialing out and ringing in
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time

from savemysugar.cumulative_average import cumulative_average
from savemysugar.Modem import Modem


class Ring(object):
    time = -1


def on_ring():
    if Ring.time == -1:
        Ring.time = time.time()
        raise StopIteration("Got the ring we were waiting for")


def measure_call_setup_time(ingoing_port, outgoing_port, destination_number):
    ingoing_modem = Modem(ingoing_port)
    ingoing_modem.register_callback("RING", on_ring)

    outgoing_modem = Modem(outgoing_port)

    min_call_time = 10000
    max_call_time = -1
    min_call_setup_time = 10000
    max_call_setup_time = -1

    avg_call_setup_time = 0

    for i in range(5):
        print()
        print("Call: %s" % (i + 1))
        outgoing_modem.send_command("ATDT" + destination_number + ";")
        dial_time = time.time()

        # Wait for the receiver to get a ring before terminating the call
        try:
            ingoing_modem.get_response_loop()
        except (StopIteration, KeyboardInterrupt):
            outgoing_modem.send_command("ATH")
            outgoing_modem.get_response()

        hangup_time = time.time()

        call_setup_time = Ring.time - dial_time
        min_call_setup_time = min(min_call_setup_time, call_setup_time)
        max_call_setup_time = max(max_call_setup_time, call_setup_time)
        avg_call_setup_time = cumulative_average(avg_call_setup_time,
                                                 i + 1,
                                                 call_setup_time)
        print("Call setup time: %f" % call_setup_time)
        print("Min call setup time: %f" % min_call_setup_time)
        print("Max call setup time: %f" % max_call_setup_time)
        print("Average call setup time: %f" % avg_call_setup_time)
        print()

        call_time = hangup_time - dial_time
        min_call_time = min(min_call_time, call_time)
        max_call_time = max(max_call_time, call_time)
        print("Total call time: %f" % call_time)
        print("Min call time: %f" % min_call_time)
        print("Max call time: %f" % max_call_time)
        print()

        Ring.time = -1

        # When dialing with an analog modem I noticed that if calls are
        # separated one from another they take less time to set up, this
        # may be due to how an analog modem works: getting it on-hook and
        # off-hook takes quite some time.
        inter_call_sleep = 5.2
        print("Sleeping %.2f sec before the next call..." % inter_call_sleep)
        time.sleep(inter_call_sleep)


def main():
    import sys
    if len(sys.argv) != 4:
        print("usage: %s" % sys.argv[0],
              "<ingoing serial port> <outgoing serial port>",
              "<destination number>")
        sys.exit(1)

    measure_call_setup_time(sys.argv[1], sys.argv[2], sys.argv[3])


if __name__ == "__main__":
    main()
