#!/usr/bin/env python3
#
# SaveMySugar - receive messages using Morse code via phone rings
#
# Copyright (C) 2015  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from savemysugar.CallDistanceTransceiver import CallDistanceTransceiver
from savemysugar.Modem import Modem

# TODO: set the logging level from the command line
FORMAT = "%(created)f %(levelname)s:%(funcName)s %(message)s"
logging.basicConfig(level=logging.DEBUG, format=FORMAT)


def receive(port):
    modem = Modem(port)

    receiver = CallDistanceTransceiver(modem)

    while True:
        receiver.receive_loop()
        print()
        print("Message received!")
        print(receiver.get_text(), flush=True)


def main():
    print("Available hardware serial ports:")
    for port in Modem.scan():
        print(("  - " + port))
    print()

    import sys
    if len(sys.argv) != 2:
        print("usage: %s <serial port>" % sys.argv[0])
        sys.exit(1)

    receive(sys.argv[1])


if __name__ == "__main__":
    main()
